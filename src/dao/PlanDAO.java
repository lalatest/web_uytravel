
package dao;

import java.util.ArrayList;
import java.util.List;

import model.Plan;

public class PlanDAO{

    //登録した旅行プランをすべてを呼び出す
    public List<Plan> findAll(){
      //複数の旅行プランを登録するリスト
    	//ArrayList <型 //int:Integer> 変数名 = new ArrayList<型/省略可>();
        List<Plan> planList = new ArrayList<>();
        //リストにインスタンスを登録
        planList.add(new Plan(1,"ウルル",20,"1.jpg","満天の星空、南十字星、ウルルで朝日鑑賞、BBQ、プール、あとは好きにしてください。"));
        planList.add(new Plan(2,"シドニー",15,"2.jpg","サーキュラーキー、ダーリングハーバー、ボンダイ、マンリー、動物園、あとは好きにしてください。"));
        planList.add(new Plan(3,"パース",10,"3.jpg","パース、フリーマントルでFish&Chips、ロットネス島でクオッカと遊ぶ、あとは好きにしてください。"));
        planList.add(new Plan(4,"ゴールドコースト",7,"4.jpg","買い物、サーフィン、ナイトライフ、あとは好きにしてください。"));
        planList.add(new Plan(5,"ケアンズ",5,"5.jpg","グレードバリアリーフ、キュランダ、あとは好きにしてください。"));
        planList.add(new Plan(6,"メルボルン",15,"6.jpeg","カフェ巡り、グレートオーシャンロード、フィリップ島でペンギンに会う、あとは好きにしてください。"));

        return planList;
    }

    //登録メンバーを1名呼び出す
    public Plan find(int index){
        Plan p= findAll().get(index-1);
        return p;
    }

}