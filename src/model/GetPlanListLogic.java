
package model;

import java.util.List;

import dao.PlanDAO;

public class GetPlanListLogic {
    public List<Plan> execute(){
        PlanDAO dao = new PlanDAO();
        return dao.findAll();
    }
}