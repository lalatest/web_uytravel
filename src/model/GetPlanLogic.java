
package model;

import dao.PlanDAO;

public class GetPlanLogic {
    public Plan execute(int index){
        PlanDAO dao = new PlanDAO();
        return dao.find(index);
    }

}