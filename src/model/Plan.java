
package model;

import java.io.Serializable;

public class Plan implements Serializable {
    private int id;
    private String name;
    private int price;
    private String imgSrc;
    private String comment;

    public Plan(){}
    /**
     * @param id
     * @param name
     * @param price
     * @param imgSrc
     * @param comment
     */
    public Plan(int id, String name, int price, String imgSrc, String comment) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imgSrc = imgSrc;
        this.comment = comment;
    }
    //ゲッター・セッター自動生成
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getImgSrc() {
		return imgSrc;
	}
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}



}