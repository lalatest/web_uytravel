package servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GetPlanListLogic;
import model.GetPlanLogic;
import model.Plan;

/**
 * Servlet implementation class MemberList
 */
@WebServlet("/plan")
public class PlanList extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //フォワード設定
        RequestDispatcher dispatcher;
        String forwardPath = "";
        // パラメータにメンバーIDが付与されているかチェック
        String id = request.getParameter("id");
        if (id == null || id.length() == 0) {
            // ID付与されていない場合
            GetPlanListLogic gpll = new GetPlanListLogic();
            // メンバーリストを取得
            List<Plan> planList = gpll.execute();
            request.setAttribute("planList", planList);
            // フォワード先
            forwardPath = "/WEB-INF/jsp/plan_list.jsp";

        } else {
            // メンバーIDを数値で取得
            int index = Integer.parseInt(id);
            GetPlanLogic gpl = new GetPlanLogic();
            Plan p = gpl.execute(index);
            request.setAttribute("plan", p);
            // フォワード先
            forwardPath = "/WEB-INF/jsp/plan.jsp?id=" + index;
        }
        // フォワード処理
        dispatcher = request.getRequestDispatcher(forwardPath);
        dispatcher.forward(request, response);
    }

}