
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/common.jsp"%>
<c:set var="pageName">HOME</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS  -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<link rel="stylesheet" href="css/style.css">
<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<title>${siteName}| ${pageName}</title>
<link>
</head>
<body>
	<!--ページヘッダー  -->
	<header class="container-fruid">
		<%@ include file="/WEB-INF/include/header.jsp"%>
	</header>
	<!--ページヘッダー//  -->
	<!-- ページコンテンツ -->
	<!-- ジャンボトロン -->
	<div class="banner_img jumbotron jumbotron-fluid">
		<div class="banner container py-4 my-4">
			<h1 class="display-4">${siteName}</h1>
			<p class="lead">架空の旅行会社のページ</p>
		</div>
	</div>
	<!-- ジャンボトロン// -->

	<!-- 以下メインコンテンツ -->
	<article class="container my-4">
		<section class="row">
			<!--イントロダクション  -->

			<!-- pegeタイトル -->
			<h1 class="col-12 border-bottom my-4">${pageName}</h1>

			<!-- About -->
			<h2 class="subtitle col-12 my-2">
				<a name="link_about">About</a>
			</h2>
			<div class="col-4 text-center">
				<img src="img/a1.jpg" alt="旅のイメージ計画" class="img-thumbnail"
					width="400" height="300" />
				<h2 class="h2">on your own</h2>
				<p>思い通りの旅のプラン</p>
			</div>
			<div class="col-4 text-center">
				<img src="img/a2.jpg" alt="旅のイメージお金" class="img-thumbnail"
					width="400" height="300" />
				<h2 class="h2">reasonable price</h2>
				<p>手ごろな価格の旅</p>
			</div>
			<div class="col-4 text-center">
				<img src="img/a3.jpg" alt="旅のイメージ" class="img-thumbnail" width="400"
					height="300" />
				<h2 class="h2">and fun!!</h2>
				<p>旅に役立つ情報</p>
			</div>
		</section><!-- //Home & About -->


		<section class="row"><!-- Plan -->
			<h2 class="subtitle col-12 my-2">Plan</h2>


			<h3 class="sub_list_title col-12 my-2">オーストラリア特集</h3>
			<div class="box col-6 text-center">
				<img src="img/1.jpg" alt="ウルルの写真" class="rounded-circle" width="200"
					height="140" />
				<h2 class="h2">ウルル</h2>
				<p>ノーザンテリトリー</p>
				<a href="#contents1" class="btn btn-primary">Uluruへ</a>
			</div>
			<div class="box col-6 text-center">
				<img src="img/2.jpg" alt="シドニーオペラハウスの写真" class="rounded-circle"
					width="200" height="140" />
				<h2 class="h2">シドニー</h2>
				<p>ニューサウスウェールズ</p>
				<a href="#contents2" class="btn btn-primary">Sydneyへ</a>
			</div>
			<div class="box col-6 text-center">
				<img src="img/3.jpg" alt="フリーマントルの写真" class="rounded-circle"
					width="200" height="140" />
				<h2 class="h2">パース</h2>
				<p>西オーストラリア</p>
				<a href="#contents3" class="btn btn-primary">Perthへ</a>
			</div>
			<div class="box col-6 text-center">
				<img src="img/4.jpg" alt="ゴールドコーストの写真" class="rounded-circle"
					width="200" height="140" />
				<h2 class="h2">ゴールドコースト</h2>
				<p>クィーンズランド</p>
				<a href="#contents4" class="btn btn-primary">Gold Coastへ</a>
			</div>
			<div class="box col-6 text-center">
				<img src="img/5.jpg" alt="グレートバリアリーフの写真" class="rounded-circle"
					width="200" height="140" />
				<h2 class="h2">ケアンズ</h2>
				<p>クィーンズランド</p>
				<a href="#contents5" class="btn btn-primary">Carinsへ</a>
			</div>
			<div class="box col-6 text-center">
				<img src="img/6.jpeg" alt="フリンダースストリート駅の写真" class="rounded-circle"
					width="200" height="140" />
				<h2 class="h2">メルボルン</h2>
				<p>ビクトリア</p>
				<a href="#contents6" class="btn btn-primary">Melbourneへ</a>
			</div>

		</section>

		<!-- コンテンツ中身  -->
		<!-- コンテンツ1  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents1">聖地ウルルで星空観賞の旅</h2>
				<p>満天の星空、南十字星、ウルルで朝日鑑賞、BBQ、プール、あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ2  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents2">シドニーでハーバーライフを満喫</h2>
				<p>サーキュラーキー、ダーリングハーバー、ボンダイ、マンリー、動物園、あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ3  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents3">パースから西オーストラリアを楽しむ</h2>
				<p>パース、フリーマントルでFish&Chips、ロットネス島でクオッカと遊ぶ、あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ4  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents4">ゴールドコーストで散歩</h2>
				<p>買い物、サーフィン、ナイトライフ、あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ5  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents5">ケアンズで大自然に癒される</h2>
				<p>グレードバリアリーフ、キュランダ、あとは好きにしてください。</p>
			</section>
		</div>
		<!-- コンテンツ6  -->
		<div class="row my-4">
			<section class="col-12">
				<h2 class="h2" id="contents6">メルボルンで最高の休日を</h2>
				<p>カフェ巡り、グレートオーシャンロード、フィリップ島でペンギンに会う、あとは好きにしてください。</p>
			</section>
		</div>

	</article>
	<!-- ページコンテナ// -->
	<!--ページフッター  -->
	<footer class="container-fruid py-4 bg-light">
		<%@ include file="/WEB-INF/include/footer.jsp"%>
	</footer>
	<!--ページフッター//  -->
</body>
</html>