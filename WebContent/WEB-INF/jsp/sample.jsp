
<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/common.jsp"%>
<c:set var="pageName">Top</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS  -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>


<title>${pageName}:${siteName}</title>
</head>
<body>
        <!--ページヘッダー  -->
        <header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>
        <!--ページヘッダー//  -->
        <!-- ページコンテンツ -->
        <article class="container my-4 py-4">
                <section class="row">
                        <!-- 以下メインコンテンツ -->
                        <h1 class="col-12 border-bottom my-4">${pageName}</h1>

                </section>

        </article>
        <!-- ページコンテンツ// -->
        <!--ページフッター  -->
        <footer class="container-fruid py-4 bg-light">
                <%@ include file="/WEB-INF/include/footer.jsp"%>
        </footer>
        <!--ページフッター//  -->
</body>
</html>