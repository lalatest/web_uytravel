
<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/common.jsp"%>
<c:set var="pageName">${plan.name}のプラン</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS  -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>


<title>${siteName} | ${pageName}</title>
</head>
<body>
        <!--ページヘッダー  -->
        <header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>
        <!--ページヘッダー//  -->
        <!-- ページコンテンツ -->
        <article class="container my-4 py-4">
                <section class="row">
                        <!-- 以下メインコンテンツ -->
                        <h1 class="col-12 border-bottom my-4">${plan.name}</h1>
                        <div class="media col-12">
                                <img class="mr-3" src="${sitePath}/img/${plan.imgSrc}"
                                        alt="${plan.name}の画像" style="max-width: 320px;">
                                <div class="media-body">
                                        <h5 class="mt-0">${plan.name}</h5>
                                        <p>金額：${plan.price}万円から</p>
                                        <p>${plan.comment}</p>
                                </div>
                        </div>
                </section>
                <div>
                        <p class="text-right">
                                <a href="${sitePath}/plan" class="btn btn-link">プラン一覧ページに戻る</a>
                        </p>
                </div>
        </article>
        <!-- ページコンテンツ// -->
        <!--ページフッター  -->
        <footer class="container-fruid py-4 bg-light">
                <%@ include file="/WEB-INF/include/footer.jsp"%>
        </footer>
        <!--ページフッター//  -->
</body>
</html>