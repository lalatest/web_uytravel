
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/common.jsp"%>
<c:set var="pageName">旅行プラン一覧</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS  -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>
<link rel="stylesheet" href="css/style.css">
<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>


<title>${siteName} | ${pageName}</title>
</head>
<body>
	<!--ページヘッダー  -->
	<header class="container-fruid">
		<%@ include file="/WEB-INF/include/header.jsp"%>
	</header>
	<!--ページヘッダー//  -->
	<!-- ページコンテンツ -->
	<article class="container my-4 py-4">
		<section class="row">
			<!-- 以下メインコンテンツ -->
			<h1 class="col-12 border-bottom my-4">${pageName}</h1>



			<!-- 特集 AUS -->
			<h3 class="sub_list_title col-12 my-2">オーストラリア特集</h3>
			<!--メンバー一覧  -->
			<c:forEach var="plan" items="${planList}">

				<div class="card col-4">
					<img src="${sitePath}/img/${plan.imgSrc}" alt="${plan.name}の画像"
						class="card-img" style="max-width: 300px;" />
					<div class="card-body">
						<h3 class="card-title">${plan.name}</h3>
						<p class="card-text">金額：${plan.price}万円から</p>
					</div>
					<div class="card-footer">
						<a href="${sitePath}/plan?id=${plan.id}"
							class="btn btn-primary btn-block">詳細</a>
					</div>
				</div>

			</c:forEach>



			<!-- 特集 Holland -->
			<h3 class="sub_list_title col-12 my-2">オランダ特集</h3>
			<!--メンバー一覧  -->
			<c:forEach var="plan2" items="${planList}">

				<div class="card col-4">
					<img src="${sitePath}/img/${plan.imgSrc}" alt="${plan.name}の画像"
						class="card-img" style="max-width: 300px;" />
					<div class="card-body">
						<h3 class="card-title">${plan.name}</h3>
						<p class="card-text">金額：${plan.price}万円から</p>
					</div>
					<div class="card-footer">
						<a href="${sitePath}/plan?id=${plan.id}"
							class="btn btn-primary btn-block">詳細</a>
					</div>
				</div>

			</c:forEach>
		</section>

	</article>
	<!-- ページコンテンツ// -->
	<!--ページフッター  -->
	<footer class="container-fruid py-4 bg-light">
		<%@ include file="/WEB-INF/include/footer.jsp"%>
	</footer>
	<!--ページフッター//  -->
</body>
</html>